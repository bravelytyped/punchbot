from colorama import Fore, Style, init as init_colorama
from datetime import datetime

import re
import sys
import traceback

NON_ALPHANUMERIC_PATTERN = re.compile('[^A-Za-z0-9]+')

def normalize_string(the_string):
  return re.sub(NON_ALPHANUMERIC_PATTERN, '', the_string).lower()

class Logger:
  DEFAULT_DESTINATION = sys.stdout

  def __init__(self, destination=None):
    self._destination = destination if destination is not None else Logger.DEFAULT_DESTINATION

  def log_exception(self, e):
    self.log_error(f'Exception occurred')
    print(file=self._destination)
    traceback.print_exception(type(e), e, e.__traceback__, file=self._destination)
    print(file=self._destination)

  def log_error(self, message):
    self.log(self._colorize('E', Fore.RED), message)

  def log_information(self, message):
    self.log(self._colorize('I', Fore.GREEN), message)

  def log(self, severity, message):
    timestamp = datetime.utcnow().isoformat(timespec='seconds')
    print('•', severity, self._colorize(timestamp, Fore.CYAN, Style.BRIGHT), f'{message}.', file=self._destination)

  def _colorize(self, string, *colors):
    if self._destination.isatty():
      return f'{"".join(colors)}{string}{Style.RESET_ALL}'
    else:
      return string

def log_exception(e):
  Logger().log_exception(e)

def log_error(message):
  Logger().log_error(message)

def log_information(message):
  Logger().log_information(message)

init_colorama()
