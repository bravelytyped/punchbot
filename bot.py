from command import Command
from common import normalize_string, log_information
from pathlib import Path

import json
import random

class Bot:
  STATE_FILE_PATH = Path('./state.json')

  def __init__(self):
    self._state = BotState.load_json_file(Bot.STATE_FILE_PATH)

  def handle_ready(self):
    log_information('I smell pork chops')

  def handle_message(self, content):
    try:
      command, args = Command.parse(content)

    except KeyError:
      return

    log_information(f'Punching out a reply to command \'{command}\'')
    return command.run(self._state, args)

class BotState:
  @staticmethod
  def load_json_file(json_file_path):
    if not json_file_path.exists():
      log_information(f'Bot state file \'{json_file_path.resolve()}\' does not exist yet; creating a fresh state')
      return BotState(file_path=json_file_path, games={})

    log_information(f'Loading bot state from \'{json_file_path.resolve()}\'')
    with json_file_path.open('r') as f:
      document = json.load(f)

    return BotState(file_path=json_file_path, games=document['games'])

  @staticmethod
  def from_json_string(json_string):
    document = json.loads(json_string)
    return BotState(games=document['games'])

  def __init__(self, *, file_path=None, games=None):
    self._file_path = file_path
    self._games = games if games is not None else {}

  def add_game(self, game_name):
    key = normalize_string(game_name)
    self._games[key] = game_name

  def remove_game(self, game_name):
    key = normalize_string(game_name)
    del self._games[key]

  def has_game(self, game_name):
    key = normalize_string(game_name)
    return key in self._games

  def get_games(self):
    return list(self._games.values())

  def save_json_file(self):
    if self._file_path is None:
      log_information(f'Not saving bot state: non-persisted storage (file_path is None)')
      return

    document = {'games': self._games}

    log_information(f'Saving bot state to \'{self._file_path.resolve()}\'')
    with open(self._file_path, 'w') as f:
      json.dump(document, f)

  def to_json_string(self):
    document = {'games': self._games}
    return json.dumps(document)
