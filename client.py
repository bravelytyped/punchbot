from bot import Bot
from common import log_exception
from dotenv import load_dotenv
from os import getenv

import discord

class Client:
  def __init__(self):
    self._discord_client = discord.Client()
    self._bot = Bot()

    @self._discord_client.event
    async def on_ready():
      await self.handle_discord_ready()

    @self._discord_client.event
    async def on_message(discord_message):
      await self.handle_discord_message(discord_message)

  def run(self, discord_secret):
    self._discord_client.run(discord_secret)

  async def handle_discord_ready(self):
    self._bot.handle_ready()

  async def handle_discord_message(self, discord_message):
    try:
      if discord_message.author == self._discord_client.user:
        return

      reply = self._bot.handle_message(discord_message.content)

      if reply is not None and len(reply) > 0:
        await discord_message.channel.send(reply)

    except Exception as e:
      log_exception(e)

def main():
  load_dotenv()
  discord_secret = getenv('DISCORD_SECRET')

  Client().run(discord_secret)

if __name__ == '__main__':
  main()
