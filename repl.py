from argparse import ArgumentParser, FileType
from bot import Bot
from colorama import Fore, Style
from common import Logger, log_exception, log_information

class Repl:
  def __init__(self):
    self._bot = Bot()

  def run(self):
    log_information('Begin REPL session')

    print()
    print(f'{stripify("cool text art", Fore.RED, Fore.YELLOW)} ↴')
    print(f'↘ ╭┬┬┬┬╮╭───────────╮╭┬┬┬┬╮')
    print(f'  │╲╱╲╱┼╡ {Fore.YELLOW}{Style.BRIGHT}Punch Bot{Style.RESET_ALL} ╞┼╲╱╲╱│')
    print(f'  ╰┈┈┈┈╯╰───────────╯╰┈┈┈┈╯ ↰')
    print(f'                      ↑ {stripify("wow so neat", Fore.BLUE, Fore.CYAN, Fore.MAGENTA)}')
    print()
    print(f'Type {Fore.MAGENTA}!quit{Style.RESET_ALL} or press {Fore.MAGENTA}CTRL+C{Style.RESET_ALL} to exit')
    print()

    self._bot.handle_ready()

    while True:
      try:
        message_content = input(f'> {Fore.CYAN}Input{Style.RESET_ALL}: ')

      except (EOFError, KeyboardInterrupt):
        break

      if message_content == '!quit':
        break

      self.handle_repl_message(message_content)

    log_information('End REPL session')
    print('Bye!')

  def handle_repl_message(self, message_content):
    try:
      reply = self._bot.handle_message(message_content)

      if reply is not None and len(reply) > 0:
        print(f'< {Fore.BLUE}Reply{Style.RESET_ALL}:', reply)
        print()

    except Exception as e:
      log_exception(e)

def main():
  arguments = parse_arguments()
  Logger.DEFAULT_DESTINATION = arguments.log_file
  Repl().run()

def parse_arguments():
  parser = ArgumentParser()
  parser.add_argument('-f', '--log-file', default='punchbot.log', type=FileType('w', encoding='utf8'))
  return parser.parse_args()

def stripify(string, *colors):
  result = ''
  i = 0

  for c in string:
    result += f'{colors[i]}{c}{Style.RESET_ALL}'
    i = (i + 1) % len(colors)

  return result

if __name__ == '__main__':
  main()
