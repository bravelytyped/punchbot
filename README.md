# Punch Bot

Punch Bot is a Discord bot. It's mainly for randomly choosing games for indecisive people, but can
also roll dice, say hi, and summon the Punch Squad.

## Dependencies

Punch Bot runs on Python 3, at least version 3.6.8.

A few third-party packages are used:

* `colorama` >= 0.4.4 for colorized terminal output
* `discord.py` >= 1.7.3 for Discord interaction
* `python-dotenv` >= 0.19.2 for configuration

## Setup

1. If you haven't already, install [Python]
2. Create a Python virtual environment using `python3 -m venv ./venv`
3. Install the required packages using `./venv/bin/pip install -r ./requirements.txt`

## Usage

Punch Bot has two modes of operation: a command-line REPL (read/eval/print/loop) for testing, and a
Discord client application for running the bot on an actual server.

The REPL can be run using the `repl.py` script: `./venv/bin/python ./repl.py`

The Discord client needs a bit of additional setup. Ultimately, you need to do the following:

1. Invite the bot to your server
2. Get the bot's OAuth2 token from the Discord developer portal
3. Create a file in the repository root named `.env`
4. Add a single line to the `.env` file: `DISCORD_SECRET=mysupersecrettoken` (replace with the actual token)
5. Run `./venv/bin/python ./client.py`

## Contributors

Developed by bravelytyped, mantaraygun, and Xeltide.

[Python]: https://www.python.org/downloads/
