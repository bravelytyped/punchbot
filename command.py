from common import log_information

import random

class Command:
  PREFIX = '!'

  REGISTRY = {}

  DEFAULT = None

  @classmethod
  def register(cls, command_class):
    assert command_class.KEY not in cls.REGISTRY
    cls.REGISTRY[command_class.KEY] = command_class
    return command_class

  @classmethod
  def register_default(cls, command_class):
    assert cls.DEFAULT is None
    cls.DEFAULT = command_class
    return command_class

  @classmethod
  def parse(cls, content):
    content = content.strip()
    parts = content.split()

    if len(parts) == 0:
      raise KeyError('Not a command (empty or all whitespace)')

    command_name = parts[0].lower()
    command_args = parts[1:]

    if not command_name.startswith(cls.PREFIX):
      raise KeyError(f'Not a command (missing prefix): {command_name}')

    command_name = command_name[1:]
    command_class = cls.REGISTRY.get(command_name, cls.DEFAULT)

    return command_class(), command_args

@Command.register
class Hello:
  KEY = 'hello'

  def run(self, state, args):
    return 'Hello! Hey there. How are ya. How are you doing today.'

@Command.register
class AddGame:
  KEY = 'add-game'

  def run(self, state, args):
    game_name = ' '.join(args)

    if state.has_game(game_name):
      return f'Hey! I already have {game_name}! I\'m calling the police!'

    log_information(f'Adding game \'{game_name}\'')
    state.add_game(game_name)
    state.save_json_file()

    return f'Okay, I added \'{game_name}\'.'

@Command.register
class RemoveGame:
  KEY = 'remove-game'

  def run(self, state, args):
    game_name = ' '.join(args)

    if not state.has_game(game_name):
      return f'Hey! {game_name} isn\'t in my list! I\'m calling the mafia!'

    log_information(f'Removing game \'{game_name}\'')
    state.remove_game(game_name)
    state.save_json_file()

    return f'Okay, I deleted \'{game_name}\'.'

@Command.register
class ChooseGame:
  KEY = 'choose-game'

  def run(self, state, args):
    games = state.get_games()

    if len(games) == 0:
      return f'Sorry champ, ain\'t got anything to choose from! Add some games with {Command.PREFIX}{AddGame.KEY}!'

    options = ', '.join(games)
    choice = random.choice(games)
    return f'Out of {options}, I choose: {choice}.'

@Command.register
class Roll:
  KEY = 'roll'

  def run(self, state, args):
    rolls = []

    for die in args:
      die = die.lower()

      if not die.startswith('d'):
        rolls.append(f'* Hey! {die} isn\'t a die!')
        continue

      try:
        sides = int(die[1:])

      except ValueError:
        rolls.append(f'* Hey! {die} isn\'t a die!')
        continue

      rolls.append(f'* For the {die}, I rolled: {random.randint(1, sides)}')

    rolls_list = '\n'.join(rolls)
    return f'Results:\n\n{rolls_list}'

@Command.register
class PunchSignal:
  KEY = 'punch-signal'

  ROLE_ID = 773014615746674734

  def run(self, state, args):
    return f'<@&{PunchSignal.ROLE_ID}>, assemble!'

@Command.register_default
class Unknown:
  def run(self, state, args):
    return 'Didn\'t quite catch that. Try better, I guess?'
